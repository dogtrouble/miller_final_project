﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomRowScript : MonoBehaviour {
    public BoardScript boardManager;
    public ClickManager clickManager;
    public bool placeable;
    public BottomTileScript[] tiles;
    public TileSelect tilePool;
    int numTiles;
    int maxTiles;
	// Use this for initialization
	void Start () {
        maxTiles = tiles.Length;
        placeable = false;
        numTiles = 0;
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnMouseOver()
    {
        placeable = boardManager.enableBottomRow();
        
        /*
         * If the player has a tile,
         * and they aren't out of tiles,
         * and the row isn't full,
         */
        if (clickManager.currentTileCount > 0 && numTiles < maxTiles) {

            tiles[numTiles].showTile(clickManager.currentTileColor);
        }
         /*
          * Get the leftmost tile,
          * Then set its color to the clickManager color,
          * then enable its renderer,
          * and add to the current number of tiles.
          */
    }
    private void OnMouseExit()
    {
        for (int i = numTiles; i < maxTiles; i++) {
            tiles[i].hideTile();
        }
    }

    private void OnMouseDown()
    {
        if (numTiles < maxTiles && clickManager.currentTileCount > 0) {
            tiles[numTiles].addTile();
            numTiles++;
            clickManager.currentTileCount--;
        }
    }

    public void AddTile(string c) {

        tiles[numTiles].addTile();
        tiles[numTiles].setColor(c);
        numTiles++;
    }

    public int EndRound() {
        int total = 0;
        for (int i = 0; i < maxTiles; i++) {
            total += tiles[i].myScore();
            if (tiles[i].hasTile) {
                if (tiles[i].color != "firstTile") tilePool.AddBox(tiles[i].removeTile());
                else tiles[i].removeTile();
            }
        }
        /*
         * Clear the row for the next round.
         * For each tile in the array,
         * If the spot hasTile,
         * Add its value to a running total,
         * then return that value.
         */
        numTiles = 0;

        return -1 * total;
        /* We also need to return a list of tiles that were full,
         * So they can be added to the box for the next round.
         */
    }
}
