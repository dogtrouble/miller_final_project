﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickManager : MonoBehaviour {
    public GameObject currentTile;
    public string currentTileColor;
    public int currentTileCount;
    public int playerTurn;
    public bool picked;
    public bool gameOver;
    public Vector3 temp;

    public float distToCamera;

    private Camera cam;

    public List<BoardScript> players;
    public FactoryScript theFactory;
    public int nextNextPlayer;

    // Use this for initialization
	void Start () {
        cam = Camera.main;
        picked = false;
        playerTurn = Random.Range(0, players.Count);
        nextNextPlayer = (playerTurn + 1) % 2;
        print(players.Count + " " + playerTurn);
        nextPlayerTurn();
	}

    // Update is called once per frame
    void Update() {

        
        // Check for space, which debugs to end turn
        if (Input.GetKeyDown("space")) {
            for (int i = 0; i < players.Count; i++) {
                players[i].OnRoundEnd();
            }
        }
        temp = Input.mousePosition;
        temp.z = distToCamera;
        this.transform.position = cam.ScreenToWorldPoint(temp);



        if (currentTileCount <= 0 && picked)
        {
            picked = false;
            nextPlayerTurn();
        }
        else if (currentTileCount <= 0) {
            currentTile.GetComponent<MeshRenderer>().enabled = false;
        }
        else if (currentTileCount > 0)
        {
            currentTile.GetComponent<TileColorManager>().SetColor(currentTileColor);
            currentTile.GetComponent<MeshRenderer>().enabled = true;
        }

        if (theFactory.canMoveOn() && currentTileCount <=0) {
            for (int i = 0; i < players.Count; i++) {
                players[i].OnRoundEnd();
            }
            for (int i = 0; i < players.Count; i++) {
                if (players[i].rowsFinished() > 0) {
                    onGameEnd();
                    print("GAME OVER!");
                    break;
                }
            }

            theFactory.OccupyAllFactories();
            playerTurn = nextNextPlayer - 1;
            nextPlayerTurn();
            nextNextPlayer = -1;
            for (int i = 0; i < players.Count; i++) {
                if (players[i].rowsFinished() > 0) {
                    onGameEnd();
                }
            }
        }
    }

    void onGameEnd() {
        for (int i = 0; i < players.Count; i++) {
            players[i].disableAllRows();
            players[i].score += (players[i].rowsFinished() * 2);
            players[i].score += players[i].colsFinished() * 7;
            players[i].score += players[i].colorsFinished() * 10;
        }
        gameOver = true;
    }

    public int winner() {
        int maxScore = 0;
        int win = -1;
        for (int i = 0; i < players.Count; i++) {
            if (players[i].score > maxScore) {
                maxScore = players[i].score;
                win = i;
            }
        }
        return win;
    }

    void nextPlayerTurn() {
        
        playerTurn = (playerTurn + 1) % players.Count;
        for (int i = 0; i < players.Count; i++) {
            if (i == playerTurn)
            {
                players[i].enableAllRows();
            }
            else players[i].disableAllRows();
        }
        print("player " + playerTurn + "'s turn");
    }

    public void setNextFirst(int i) {
    }
}
