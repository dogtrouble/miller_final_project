﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightSideSpot : MonoBehaviour {

    public GameObject myTile;
    public GameObject myPlane;

    public bool hasTile;
	// Use this for initialization
	void Start () {
        myTile = transform.GetChild(0).gameObject;
        myPlane = transform.GetChild(1).gameObject;
        hasTile = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setColors(string c) {
        myTile.GetComponent<TileColorManager>().SetColor(c);
        myTile.GetComponent<Renderer>().enabled = false;
        myPlane.GetComponent<TileColorManager>().SetColor(c);
    }

    public bool isTileEnabled() {
        return hasTile;
    }

    public void placeTile() {
        myTile.GetComponent<Renderer>().enabled = true;
        hasTile = true;
    }
}
