﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftScript : MonoBehaviour {

    TileColorManager tileCol;
    MeshRenderer rend;

	// Use this for initialization
	void Awake () {
        tileCol = transform.Find("Tile").GetComponent<TileColorManager>();
        rend = transform.Find("Tile").GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void onHover(string color) {
        tileCol.SetColor(color);
        tileCol.makeTransparent();
        rend.enabled = true;
    }
    public void onClick(string color) {
        tileCol.SetColor(color);
        tileCol.unMakeTransparent();
        rend.enabled = true;
    }
    public void OnExit()
    {
        rend.enabled = false;
    }
}
