﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverText : MonoBehaviour {


    public ClickManager cm;
    Text myText;
	// Use this for initialization
	void Start () {
        myText = GetComponent<Text>();
        myText.text = "";
	}
	
	// Update is called once per frame
	void Update () {
        if (cm.gameOver) {
            myText.text = "Game Over!\nPlayer " + (cm.winner() + 1) + " wins!";
        }
	}
}
