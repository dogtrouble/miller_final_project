﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileClicker : MonoBehaviour {
    public bool hasTile;
    public string tileColor;
    public ClickManager cm;
    public FactorySpotScript parent;
    TileColorManager manager;
    GameObject myObject;
    public MeshRenderer myRend;
	// Use this for initialization
	void Start () {
        
	}
    private void Awake()
    {
        myObject = this.gameObject;
        manager = myObject.GetComponent<TileColorManager>();
        myRend = myObject.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void setColor(string c) {
        manager.SetColor(c);
        this.tileColor = c;
        enableTile();
    }
    public void OnMouseDown()
    {
        if (cm.currentTileCount == 0 && hasTile && tileColor != "firstTile") {
            parent.onTilePickup(tileColor);
            cm.picked = true;
            // add tiles to clickManager current tiles by calling parent function in factorySpot
        }
    }

    public void removeTile() {
        myRend.enabled = false;
        hasTile = false;
        tileColor = "";
    }

    public void enableTile() {
        myRend.enabled = true;
        hasTile = true;
    }

}
