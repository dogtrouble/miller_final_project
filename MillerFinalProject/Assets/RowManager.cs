﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowManager : MonoBehaviour {
    public GameObject[] leftSide;
    LeftScript[] lefts;
    public GameObject[] rightSide;
    public ClickManager clickManager;

    public RightSideSpot[] rightScripts = new RightSideSpot[5];

    GameObject currentTile;
    RightSideSpot thisSpot;
    public bool canPlaceHere;
    public string rowColor;
    public int numberTiles; //Current number of tiles in the row
    public int maxTiles; //Max number of tiles in the row, also row number
    public int tilesCompleted; //Number of tiles completed, i.e. on the right side

    int blueSpot;
    int yellowSpot;
    int redSpot;
    int blackSpot;
    int whiteSpot;
    string[] colors;

    public TileSelect poolManager;

    private void Start()
    {
        for (int i = 0; i < maxTiles; i++) {
            leftSide[i].transform.Find("Plane").GetComponent<TileColorManager>().SetColor("firstTile");
            
        }
        tilesCompleted = 0;
        lefts = new LeftScript[leftSide.Length];
        for (int i = 0; i < leftSide.Length; i++) {
            lefts[i] = leftSide[i].GetComponent<LeftScript>();
        }
    }

    // Use this for initialization
    void Awake () {
        canPlaceHere = true;
        // The spots for each tile will correspond to this.
        // Since maxTiles = row number,
        //  the offset will change for every row.
        blueSpot = (maxTiles - 1) % 5;
        yellowSpot = (maxTiles) % 5;
        redSpot = (maxTiles + 1) % 5;
        blackSpot = (maxTiles + 2) % 5;
        whiteSpot = (maxTiles + 3) % 5;

        colors = new string[5];
        colors[blueSpot] = "blue";
        colors[yellowSpot] = "yellow";
        colors[redSpot] = "red";
        colors[blackSpot] = "black";
        colors[whiteSpot] = "white";

        for (int i = 0; i < 5; i++) {
            
            /*
            thisSpot = rightSide[i].GetComponent<RightSideSpot>();
            thisSpot.setColors(colors[i]);
            */
            rightScripts[i] = rightSide[i].GetComponent<RightSideSpot>();
            rightScripts[i].setColors(colors[i]);
            /*
            currentTile = rightSide[i].transform.GetChild(0).gameObject;
            currentTile.GetComponent<TileColorManager>().SetColor(colors[i]);
            currentTile.GetComponent<Renderer>().enabled = false;
            currentTile = rightSide[i].transform.GetChild(1).gameObject;
            currentTile.GetComponent<TileColorManager>().SetColor(colors[i]);
            */
        }
	}

    bool canBeOccupied(string possibleColor) {
        int position = 5;
        for (int i = 0; i < 5; i++) {
            if (colors[i] == possibleColor) {
                position = i;
            }
        }
        //return (!rightSide[position].transform.GetChild(0).gameObject.GetComponent<Renderer>().enabled);
 
        return (!rightScripts[position].isTileEnabled());
    }

	// Update is called once per frame
	void Update () {
        
	}
    private void OnMouseOver()
    {
        /*
         * If you have a tile,
         * and you aren't out of tiles, 
         * and the row isn't full,
         * and the right side doesn't have the current color of tile,
         */
        if (clickManager.currentTile != null &&
             clickManager.currentTileCount > 0 &&
             numberTiles < maxTiles &&
             canBeOccupied(clickManager.currentTileColor) &&
             (rowColor == clickManager.currentTileColor || rowColor == ""))
        {
            /*
             * Get the leftmost tile,
             * Then set its color to the clickmanager color,
             * Then enable its renderer.
             */
            canPlaceHere = true;
            lefts[numberTiles].onHover(clickManager.currentTileColor);
            /*
            currentTile = leftSide[numberTiles].transform.GetChild(0).gameObject;
            currentTile.GetComponent<TileColorManager>().SetColor(clickManager.currentTileColor);
            currentTile.GetComponent<Renderer>().enabled = true;
            */
        }
        else canPlaceHere = false;

    }
    private void OnMouseDown()
    {
        if (clickManager.currentTile != null &&
            clickManager.currentTileCount > 0 &&
            numberTiles < maxTiles &&
            canBeOccupied(clickManager.currentTileColor) &&
            (rowColor == clickManager.currentTileColor || rowColor == "")) {


            lefts[numberTiles].onClick(clickManager.currentTileColor);
            /*
            currentTile = leftSide[numberTiles].transform.GetChild(0).gameObject;
            currentTile.GetComponent<TileColorManager>().SetColor(clickManager.currentTileColor);
            currentTile.GetComponent<Renderer>().enabled = true;
            */
            numberTiles++;
            clickManager.currentTileCount--;
            rowColor = clickManager.currentTileColor;
        }
    }
    private void OnMouseExit()
    {
        if (numberTiles < maxTiles)
        {
            lefts[numberTiles].OnExit();
            /*
            currentTile = leftSide[numberTiles].transform.GetChild(0).gameObject;
            currentTile.GetComponent<Renderer>().enabled = false;
        */

        }
    }

    public int placeTile() {
        if (numberTiles == maxTiles) {
            tilesCompleted++;
            int position = 5;
            for (int i = 0; i < 5; i++) {
                if (colors[i] == rowColor) {
                    position = i;
                }
            }
            for (int i = 1; i < maxTiles; i++)
            {
                poolManager.AddBox(rowColor);
            }
            rightScripts[position].placeTile();
            rowColor = "";
            numberTiles = 0;
            for (int i = 0; i < lefts.Length; i++)
            {
                lefts[i].OnExit();
                /*
                currentTile = leftSide[i].transform.GetChild(0).gameObject;
                currentTile.GetComponent<Renderer>().enabled = false;
            */

            }



            
            return position;
        }
        return 5;
    }


    public int EndRound() {
        if (numberTiles == maxTiles)
        {
            tilesCompleted = tilesCompleted + 1;
            print(tilesCompleted);
            //Clear the row for the next round
            int position = 5;
            for (int i = 0; i < 5; i++)
            {
                if (colors[i] == rowColor)
                {
                    position = i;
                }
            }
            rightScripts[position].placeTile();
            rowColor = "";
            numberTiles = 0;
            for (int i = 0; i < lefts.Length; i++)
            {
                /*
                currentTile = leftSide[i].transform.GetChild(0).gameObject;
                currentTile.GetComponent<Renderer>().enabled = false;
                */
                lefts[i].OnExit();
            }
            return (rowTotal(position));
        }
        return 0;
    }
    public int rowTotal(int position) {
        int total = 0;
        bool goingRight = true;
        bool goingLeft = true;
        /*
        for (int i = position; i < 5; i++) {
            print("checking position" + position);
            if (rightSide[i].transform.GetChild(0).gameObject.GetComponent<Renderer>().enabled == true)
            {
                total++;
            }
        }
        for (int i = position - 1; 0 < i; i--) {
            if (rightSide[i].transform.GetChild(0).gameObject.GetComponent<Renderer>().enabled == true)
            {
                total++;
            }
            else break;
        }
        return total;
        */
        int rightPos = position;
        int leftPos = position - 1;
        while (goingRight && rightPos < 5) {
            if (rightScripts[rightPos].hasTile)
            {
                total++;
            }
            else goingRight = false;
            rightPos++;
        }
        while (goingLeft && leftPos > 0) {
            if (rightScripts[leftPos].hasTile)
            {
                total++;
            }
            else goingLeft = false;
            leftPos--;
        }
        return total;
    }
    public bool col(int spot) {
        return (rightScripts[spot].hasTile);
    }
}
