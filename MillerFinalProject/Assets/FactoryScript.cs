﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class FactoryScript : MonoBehaviour {

    public List<FactorySpotScript> spots;
    public MiddleScript middleSpot;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("p"))
        {
            //Occupy the factory
            OccupyAllFactories();
        }
    }

    public void OccupyAllFactories()
    {
        for (int i = 0; i < spots.Count; i++)
        {
            spots[i].OccupyWithTiles();
        }
        middleSpot.addTile("firstTile");
    }

    public void addToMiddle(string c) {
        middleSpot.addTile(c);
    }

    public bool canMoveOn() {
        for (int i = 0; i < spots.Count; i++) {
            if (spots[i].tileCount != 0) {
                return false;
            }
        }
        if (middleSpot.numberTiles != 0) {
            return false;
        }
        return true;
    }
}
