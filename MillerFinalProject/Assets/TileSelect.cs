﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSelect : MonoBehaviour {


    

    public List<string> tileBag; // tiles start here and return here once the bag is empty
    public List<string> tileBox; // tiles go here after leaving any board
    public List<string> myArray;

    int numBag;
    int numBox;

	// Use this for initialization
	void Start () {
        
        myArray = new List<string>();

        
        for (int i = 0; i < 100; i++) {
            if (i % 5 == 0) tileBag.Add("black");
            else if (i % 5 == 1) tileBag.Add("blue");
            else if (i % 5 == 2) tileBag.Add("red");
            else if (i % 5 == 3) tileBag.Add("white");
            else if (i % 5 == 4) tileBag.Add("yellow");
        }

        tileBag = RandomizeList(tileBag);

        
        numBag = 100;
        numBox = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    List<string> RandomizeList(List<string> list) {
        List<string> product = new List<string>();
        int spot = 0;
        while (list.Count > 0) {
            spot = Random.Range(0, list.Count);
            product.Add(list[spot]);
            list.RemoveAt(spot);
        }
        return product;
    }
    void RandomizeArray(string[] arr) {
        for (var i = arr.Length - 1; i > 0; i--)
        {
            var r = Random.Range(0, i);
            var tmp = arr[i];
            arr[i] = arr[r];
            arr[r] = tmp;
        }
    }
    public void AddBox(string color) {
        tileBox.Add(color);
        numBox++;
    }

    public string removeTile() {
        string s = "";
        s = tileBag[numBag - 1];

        if (numBag > 0)
        {
            tileBag.RemoveAt(numBag - 1);
            numBag--;
        }
        else
        { //shuffle the bag!
            shuffleBag();
        }
        return s;
    }

    public void shuffleBag() {
        numBag = numBox;
        numBox = 0;
        // add all tiles in the box to the bag.

        while (tileBox.Count > 0) {
            tileBag.Add(tileBox[0]);
            tileBox.RemoveAt(0);
        }
        tileBag = RandomizeList(tileBag);
    }
    
}
