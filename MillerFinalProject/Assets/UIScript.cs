﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {
    public BoardScript[] players;
    int numPlayers;
    public ClickManager cm;
    Text myText;
    string scoreKeeper;
	// Use this for initialization
	void Start ()
    {
        numPlayers = players.Length;
        myText = GetComponent<Text>();
        
	}

    // Update is called once per frame
    void Update() {
        scoreKeeper = "";
        scoreKeeper += ("Player " + (cm.playerTurn+1) + "'s turn.\n");
        for (int i = 0; i < players.Length; i++) {
            scoreKeeper += ("Player " + (i+1) + " score: " + players[i].score + "\n");
        }
        if (cm.currentTileCount > 0)
        {
            scoreKeeper += ("Holding " + cm.currentTileCount + " " + cm.currentTileColor + " tiles");
        }
        else {
            scoreKeeper += ("Holding no tiles");
        }
        myText.text = scoreKeeper;
    }
}
