﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpotScript : MonoBehaviour {
	public ClickManager clickManager;
    public GameObject thisTile;

    bool hasTile;
    string tileColor;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnMouseOver()
    {
        if (clickManager.currentTile != null && clickManager.currentTileCount > 0 && !hasTile) {
            thisTile.GetComponent<Renderer>().enabled = true;
            thisTile.GetComponent<TileColorManager>().SetColor(clickManager.currentTileColor);
        }
    }
    private void OnMouseExit()
    {
        if (!hasTile) {
            thisTile.GetComponent<Renderer>().enabled = false;
        }
    }
}
