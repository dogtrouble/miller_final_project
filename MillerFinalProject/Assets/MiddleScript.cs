﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiddleScript : FactorySpotScript {

    public int numberTiles;
    Queue<int> toBePlaced;
    public bool firstHere; // Is the first player tile in here?
    // Use this for initialization
    void Start()
    {
        firstHere = true;
        numberTiles = 0;
        toBePlaced = new Queue<int>();
        for (int i = 0; i < myTiles.Length; i++) {
            myTiles[i].removeTile();
            toBePlaced.Enqueue(i);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void onTilePickup(string color)
    {
        int total = 0;
        for (int i = 0; i < myTiles.Length; i++)
        {
            if (myTiles[i].tileColor == color)
            {
                total++;
                numberTiles--;
                myTiles[i].removeTile();
                toBePlaced.Enqueue(i);
            }
            else if (myTiles[i].tileColor == "firstTile")
            {
                myTiles[i].removeTile();
                cm.players[cm.playerTurn].bottomRow.AddTile("firstTile");
                cm.nextNextPlayer = cm.playerTurn;
                numberTiles--;
                toBePlaced.Enqueue(i);
                // Add to bottom row, mark that player as next first player
            }
            else
            {
                // Don't add to middle, because this is the middle!
            }
            
        }
        cm.currentTileColor = color;
        cm.currentTileCount = total;


    }

    public void addTile(string color) {
        numberTiles++;
        if (numberTiles > myTiles.Length) return;
        myTiles[toBePlaced.Dequeue()].setColor(color);
    }
}
