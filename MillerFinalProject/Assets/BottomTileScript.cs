﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomTileScript : MonoBehaviour {

    public GameObject myTile;
    public GameObject myPlane;
    public bool hasTile;
    public string color;
    public int spaceValue;
	// Use this for initialization
	void Start () {
        hasTile = false;
        color = "NOT A COLOR";

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void setColor(string color) {
        myTile.GetComponent<TileColorManager>().SetColor(color);
        this.color = color;
    }

    public void showTile() {
        myTile.GetComponent<Renderer>().enabled = true;
    }
    public void showTile(string color) {
        myTile.GetComponent<TileColorManager>().SetColor(color);
        myTile.GetComponent<Renderer>().enabled = true;
        this.color = color;
    }
    public void hideTile() {
        myTile.GetComponent<Renderer>().enabled = false;
    }

    public void addTile() {
        myTile.GetComponent<Renderer>().enabled = true;
        hasTile = true;
    }
    public string removeTile() {
        hasTile = false;
        hideTile();
        string returner = color;
        color = "";
        return returner;

    }

    public void addTile(string color) {
        myTile.GetComponent<TileColorManager>().SetColor(color);
        myTile.GetComponent<Renderer>().enabled = true;
        hasTile = true;
        this.color = color;
    }
    public int myScore() {
        if (hasTile) return spaceValue;
        else return 0;
    }
}
