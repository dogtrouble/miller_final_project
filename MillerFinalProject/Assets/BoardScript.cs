﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardScript : MonoBehaviour {

    public GameObject[] rows;
    public int score;

    public bool flag;

    public TileSelect tileSelector;
    public ClickManager clickMan;
    public BottomRowScript bottomRow;
    public RowManager[] rowScripts;
    public RightSideSpot[,] board;
    public RightSideSpot spot;
    bool isPossibleToPlace;
    // TileSelect poolManager;

    public void disableAllRows() {
        for (int i = 0; i < rowScripts.Length; i++) {
            rowScripts[i].gameObject.GetComponent<Collider>().enabled = false;
        }
        
    }

    public void enableAllRows() {
        for (int i = 0; i < rowScripts.Length; i++) {
            rowScripts[i].gameObject.GetComponent<Collider>().enabled = true;
        }
    }
    private void Awake()
    {
        isPossibleToPlace = false;
        score = 0;
        rowScripts = new RowManager[5];
        board = new RightSideSpot[5, 5];
        for (int i = 0; i < 5; i++)
        {

            rowScripts[i] = rows[i].GetComponent<RowManager>();
            rowScripts[i].poolManager = tileSelector;
            for (int j = 0; j < 5; j++)
            {
                board[i, j] = rowScripts[i].rightScripts[j];
                
            }

        }
    }
    // Use this for initialization
    void Start () {
        
	}

    /* 
     * int position;
     * for (int i = 0; i < 5; i++) {
     * position = rowScripts[i].placeTile()
     * if (position < 5) {
     * score += rowScripts[i].rowTotal(position);
     * score += columnTotal(position)
     * }
     * }
     * */
    /*
     * public int columnTotal(position) {
     * 
     * }
     * */

    public void OnRoundEnd() {
        int position;
        int col;
        int row;
        for (int i = 0; i < 5; i++) {
            position = rowScripts[i].placeTile();
            if (position < 5) {
                
                row = rowScripts[i].rowTotal(position);
                col = columnTotal(position, i);
                if (row > 1 && col >= 1) {
                    col += 1;
                }
                score += (row + col);
                
            }
        }
        score += bottomRow.EndRound();
        print("Rows: " + rowsFinished() + " columns: " + colsFinished() + " colors: " + colorsFinished());
    }

    public int rowsFinished() {
        int total = 0;
        for (int i = 0; i < 5; i++) {
            if (rowScripts[i].rowTotal(0) > 4) {
                total++;
            }
        }
        return total;
    }
    public int colsFinished() {
        int total = 0;
        for (int i = 0; i < 5; i++) {
            bool thisRow = true;
            for (int j = 0; j < 5; j++) {
                thisRow = thisRow && rowScripts[j].col(i);
            }
            if (thisRow) total++;
        }
        return total;
    }

    public int colorsFinished() {
        int total = 0;
        for (int i = 0; i < 5; i++) {
            // i = position in row one. i.e., 0=blue, 1=yellow, 2=red, etc
            bool thisColor = true;
            for (int j = 0; j < 5; j++) {
                thisColor = thisColor && rowScripts[j].rightScripts[(i + j) % 5].hasTile;
            }
            if (thisColor) total++;
        }
        return total;
    }

    int columnTotal(int position, int row) {
        int total = 0;
        int upPos = row - 1;
        int downPos = row + 1;
        bool goingUp = true;
        bool goingDown = true;

        while (goingUp && upPos >= 0) {

            spot = board[upPos, position];
            if (!spot.hasTile)
            {
                goingUp = false;
            }
            else
            {
                total++;
            }
            upPos--;
        }
        while (goingDown && downPos < 5)
        {
            if (board[downPos, position].hasTile)
            {
                total++;
            }
            else goingDown = false;
            downPos++;
        }
        
        return total;

    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space")) {
            // Check for rows
            //OnRoundEnd();
        }
        
        
    }
    public bool enableBottomRow() {
        // check and see if you can place on the bottom row
        isPossibleToPlace = false;
        for (int i = 0; i < 5; i++)
        {
            // Check for each row to see if any have possibility to place
            if (clickMan.currentTileCount > 0)
            {
                if (rowScripts[i].canPlaceHere)
                {
                    isPossibleToPlace = true;
                }
            }

        }
        return !isPossibleToPlace;
    }

    
}
