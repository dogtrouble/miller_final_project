﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileColorManager : MonoBehaviour {
    public Material blue;
    public Material black;
    public Material yellow;
    public Material white;
    public Material red;
    public Material first;



    public Material thisMaterial;

    public string color;

	// Use this for initialization
	void Start () {
        thisMaterial = this.GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SetColor(string color)
    {
        if (color == "blue") this.GetComponent<Renderer>().material = blue;
        else if (color == "black") this.GetComponent<Renderer>().material = black;
        else if (color == "yellow") this.GetComponent<Renderer>().material = yellow;
        else if (color == "white") this.GetComponent<Renderer>().material = white;
        else if (color == "red") this.GetComponent<Renderer>().material = red;
        else
        {
            this.GetComponent<Renderer>().material = first;
            print(color);
        }
    }

    public void makeTransparent() {
        Color myCol = thisMaterial.color;
        thisMaterial.color = new Color(myCol.r, myCol.g, myCol.b, .0f);
    }
    public void unMakeTransparent() {
        Color myCol = thisMaterial.color;
        thisMaterial.color = new Color(myCol.r, myCol.g, myCol.b, 1f);
    }
}


