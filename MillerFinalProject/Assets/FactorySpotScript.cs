﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactorySpotScript : MonoBehaviour {

    public TileClicker[] myTiles;
    
    public ClickManager cm;
    public TileSelect tilePool;
    public FactoryScript myParent;
    public int tileCount;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void onTilePickup(string color) {
        int total = 0;
        print("tile pickup called from factory spot");
        for (int i = 0; i < myTiles.Length; i++) {
            if (myTiles[i].tileColor == color)
            {
                total++;
                myTiles[i].tileColor = "";
            }
            else if (myTiles[i].tileColor == "firstTile") {
                // Add to bottom row, mark that player as next first player
            }
            else
            {
                myParent.addToMiddle(myTiles[i].tileColor);
            }
            myTiles[i].removeTile();
            tileCount--;
        }
        cm.currentTileColor = color;
        cm.currentTileCount = total;
        print(color + total);
        
    }

    public void OccupyWithTiles() {
        string s;
        for (int i = 0; i < 4; i++) {
            if (tilePool.tileBag.Count <= 0) tilePool.shuffleBag();
            s = tilePool.removeTile();
            myTiles[i].setColor(s);
            
            myTiles[i].enableTile();
            tileCount = 4;
        }
    }

    
}
