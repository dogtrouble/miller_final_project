﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterTileClicker : MonoBehaviour {

    public bool hasTile;
    public string tileColor;
    public ClickManager cm;
    public MiddleScript parent;

    TileColorManager manager;
    GameObject myObject;
    MeshRenderer myRend;
    // Use this for initialization
    void Start () {
        myObject = this.gameObject;
        myRend = myObject.GetComponent<MeshRenderer>();
        manager = myObject.GetComponent<TileColorManager>();

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void setColor(string c)
    {
        manager.SetColor(c);
        this.tileColor = c;
    }
    public void OnMouseDown()
    {
        if (cm.currentTileCount == 0)
        {
            parent.onTilePickup(tileColor);
            // add tiles to clickManager current tiles by calling parent function in factorySpot
        }
    }
    public void removeTile()
    {
        myRend.enabled = false;
        hasTile = false;
    }
}
